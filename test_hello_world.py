#!/usr/bin/env python
# coding=utf-8
from tornado.testing import AsyncHTTPTestCase, unittest

import hello_world


class TestHelloApp(AsyncHTTPTestCase):
    def get_app(self):
        return hello_world.make_app()

    def test_homepage(self):
        response = self.fetch('/')
        self.assertEqual(response.code, 200)
        self.assertEqual(response.body, b'Hello, world')
